<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Atomic Project</title>
	<link rel="stylesheet" href="resource/css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">Atomic Project</a></h1>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div style="padding-top:25px" class="add-gender-sub-container">
				<div class="my-info">
					<h2 style="padding-bottom:20px">BITM - Web App Dev - PHP</h2>
					<p><strong>NAME:</strong> Mohammad Emran Kabir</p>
					<p><strong>SEIP ID:</strong> 106357</p>
					<p><strong>Batch:</strong> 11</p>
				</div>
				<div class="panel panel-primary">
				  <!-- Default panel contents -->
				  <div class="panel-heading" style="text-align:center;font-weight:400; font-size:20px">Projects</div>
				  <div class="panel-body">
					<ul>
						<li>
							<button type="button" class="btn btn-success"><a href="views/SEIP106357/BookTitle/index.php">Book Title</a></button>
						</li>
						<li>
							<button style="padding: 12px 96px 12px 97px;" type="button" class="btn btn-success"><a href="views/SEIP106357/birthday/index.php">Birthday</a></button>
						</li>
						<li>
							<button style="padding: 12px 31px 12px 32px" type="button" class="btn btn-success"><a href="views/SEIP106357/SumOrg/index.php">Summary of Organization</a></button>
						</li>
						<li>
							<button style="padding: 12px 80px 12px 80px" type="button" class="btn btn-success"><a href="views/SEIP106357/Subscription/index.php">Subscription</a></button>
						</li>
						<li>
							<button style="padding: 12px 75px 12px 75px" type="button" class="btn btn-success"><a href="views/SEIP106357/ProfilePicture/index.php">Profile Picture</a></button>
						</li>
						<li>
							<button style="padding: 12px 101px 12px 101px" type="button" class="btn btn-success"><a href="views/SEIP106357/Gender/index.php">Gender</a></button>
						</li>
						<li>
							<button style="padding: 12px 53px 12px 53px" type="button" class="btn btn-success"><a href="views/SEIP106357/TermsAndConditions/index.php">Terms & Conditions</a></button>
						</li>
						<li>
							<button style="padding: 12px 104px 12px 105px" type="button" class="btn btn-success"><a href="views/SEIP106357/Hobbies/index.php">Hobby</a></button>
						</li>
						<li>
							<button style="padding: 12px 114px 12px 114px" type="button" class="btn btn-success"><a href="views/SEIP106357/city/index.php">City</a></button>
						</li>
						<li>
							<button type="button" class="btn btn-success"><a href="views/SEIP106357/Bookmark/index.php">Bookmark</a></button>
						</li>
					</ul>
				  </div>
				</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
