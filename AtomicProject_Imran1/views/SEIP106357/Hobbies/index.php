<?php
	include_once("../../../vendor/autoload.php");
	
	use Imran\BITM\SEIP106357\Hobbies\CheckboxMultiple;
	use Imran\BITM\SEIP106357\Utility\Utility;
	
	$myHobbies = new CheckboxMultiple();
	$myHobby = $myHobbies->index();
	
	
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Hobby</title>
	<link rel="stylesheet" href="css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">Hobby</a></h1>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="add-gender-sub-container">
				<h2>My Hobbies</h2>
				<div id="profile-sum-org-panel-success" class="panel panel-success">
					<div id="profile-sum-org-panel-heading" class="panel-heading">User List</div>
						<table class="table table-bordered" border="1">
						  <tr>
							<th style="text-align:center">SL</th>
							<th style="text-align:center">ID</th>
							<th style="text-align:center">Name</th> 
							<th style="text-align:center">Action</th>
						  </tr>
						  <?php
								$SL = 1;
								foreach($myHobby as $hobby){
							?>
						  <tr>
							<td><?php echo $SL;?></td>
							<td><?php echo $hobby->id;?></td>
							<td style="text-align:left"><?php echo $hobby->title;?></td> 
							<td style="text-align:center">
								<button type="button" name="button" value=""><a href="show.php?id=<?php echo $hobby->id;?>">View</a></button>
								<button type="button" name="button" value=""><a href="edit.php?id=<?php echo $hobby->id;?>">Edit</a></button>
								<button type="button" name="button" value=""><a href="deletel.php?id=<?php echo $hobby->id;?>">Delete</a></button>
							</td>
						  </tr>
						  <?php
						  $SL++;
							}
						  ?>
						</table>
				</div>
				<button type="button" name="create" value=""><a href="create.php">Create</a></button>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
