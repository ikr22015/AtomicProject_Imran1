<?php
include_once("../../../vendor/autoload.php");

	use Imran\BITM\SEIP106357\birthday\DateTime;
	use Imran\BITM\SEIP106357\Utility\Utility;



	$myBirthday = new DateTime();
	$birthdays = $myBirthday->show($_GET["id"]);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Birthday</title>
	<link rel="stylesheet" href="../../../resource/css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
	<link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">Birthday</a></h1>
      <nav class="navbar navbar-inverse">
				<ul>
					<li><a class="" href="index.php">Home</a></li>
					<li><a href="../../../index.php">Go to Ptoject Navigation Page</a></li>
				</ul>
			</nav>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="addbook-sub-container">
				<h2 style="text-align:center">Add Your Birthday</h2>
				<form action="store.php" method="post" class="form-horizontal">
					<fieldset>
						<center>
						<input type="hidden"
							name="id"
							value="<?php echo $birthdays->id;?>"
							required="required"
							/>
						<div class="from-group">
							<label class="control-label" for="email">Enter Your Name</label>
							<div>
								<input type="text" class="form-control" id="name" name="name" placeholder="Name" autofocus="autofocus" required="required" value="<?php echo $birthdays->name;?>"/>
							</div>
						</div>
						
						<div class="from-group">
							<label class="control-label" for="email">Enter Your Birthday</label>
							<div>
								<input type="date" class="form-control" id="book-title" name="day" placeholder="Book Title" required="required" value="<?php echo $birthdays->title;?>"/>
							</div>
						</div>
						
						<div class="from-group">
							<div>
								<button type="submit" class="btn btn-success" >Submit</button>
							</div>
						</div>
						</center>
					</fieldset>
				</form>
			</div>
			
			
			
			
			
			
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
