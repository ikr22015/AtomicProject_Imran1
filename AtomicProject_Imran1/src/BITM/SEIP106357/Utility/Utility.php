<?php
	namespace Imran\BITM\SEIP106357\Utility;
	session_start();

	use Imran\BITM\SEIP106357\booktitle\Text;

	class Utility{

		static public function d($peram = false){
			echo "<pre>";
			var_dump($peram);
			echo "</pre>";
		}

		static public function dd($peram = false){
			self::d($peram);
			die();
		}

		static public function redirect($url="/bitm/AtomicProjectImran/views/SEIP106357/BookTitle"){
			header("Location:".$url);
		}


		static public function message($message = null){
			if(is_null($message)){
				$_message = self::getMessage();
				return $_message;
			}else{
				self::setMessage($message);
			}
		}


		static private function getMessage(){
			$_message = $_SESSION['message'];
			$_SESSION['message'] = "";
			return $_message;
		}

		static private function setMessage($message){
			$_SESSION['message'] = $message;
		}
	}
?>
