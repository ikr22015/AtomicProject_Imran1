-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2016 at 11:26 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `imran`
--

-- --------------------------------------------------------

--
-- Table structure for table `bday`
--

CREATE TABLE IF NOT EXISTS `bday` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bday`
--

INSERT INTO `bday` (`id`, `name`, `title`) VALUES
(103, 'jhxgjhdf', '2016-01-24'),
(104, 'jhxgjhdf', '2016-01-13'),
(105, 'jhxgjhdf', '2016-01-25'),
(106, 'Nafee', '2016-01-18'),
(107, 'Imran Kabir', '2016-01-12'),
(108, 'PHP7', '2016-01-26'),
(109, 'Rajib', '2016-01-12'),
(110, 'Shumee', '2016-01-26'),
(111, 'jhxgjhdf', '2016-01-05'),
(112, 'PHP7', '2016-01-12'),
(113, 'PHP7', '2016-01-19'),
(114, 'PHP7', '2016-01-06'),
(115, 'Shumee', '2016-01-12'),
(116, 'Imran Kabir', '2016-01-04'),
(117, 'Imran Kabir', '2016-01-12'),
(118, 'Imran Kabir', '2016-01-06'),
(119, 'Imran Kabir', '2016-01-06'),
(120, 'Shumee', '2016-01-19');

-- --------------------------------------------------------

--
-- Table structure for table `birthdays`
--

CREATE TABLE IF NOT EXISTS `birthdays` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthdays`
--

INSERT INTO `birthdays` (`id`, `name`, `title`) VALUES
(48, 'Imran Kabir', '2016-01-13'),
(50, 'Nafee', '2016-01-28'),
(51, 'Imran Kabir', '2016-01-13'),
(52, 'Humaun Kabir', '2016-01-30'),
(53, 'Imran Kabir', '2016-01-31');

-- --------------------------------------------------------

--
-- Table structure for table `bookmarks`
--

CREATE TABLE IF NOT EXISTS `bookmarks` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookmarks`
--

INSERT INTO `bookmarks` (`id`, `title`) VALUES
(6, 'http://localhost/bitm/AtomicProjectImran/views/SEIP106357/Bookmark/create.php'),
(7, 'http://localhost/bitm/AtomicProjectImran/views/SEIP106357/Bookmark/create.php'),
(12, 'https://www.edx.org/');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `title`, `location`) VALUES
(16, 'Imran Kabir8877777', 'Joshor'),
(17, 'Imran Kabir', 'Joshor'),
(18, 'Imran Kabir', 'Khulna'),
(20, 'Rajib', 'Chittagong'),
(32, 'Shumee', 'Sylhet'),
(33, 'Rajib', 'Chittagong');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
  `id` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `Football` varchar(255) NOT NULL,
  `Cricket` varchar(255) NOT NULL,
  `TV` varchar(255) NOT NULL,
  `Family_Time` varchar(255) NOT NULL,
  `Fishing` varchar(255) NOT NULL,
  `Computer` varchar(255) NOT NULL,
  `Hunting` varchar(255) NOT NULL,
  `Traveling` varchar(255) NOT NULL,
  `Shopping` varchar(255) NOT NULL,
  `Sleeping` varchar(255) NOT NULL,
  `Chees` varchar(255) NOT NULL,
  `Music` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `title`, `Football`, `Cricket`, `TV`, `Family_Time`, `Fishing`, `Computer`, `Hunting`, `Traveling`, `Shopping`, `Sleeping`, `Chees`, `Music`) VALUES
(1, 'Imran Kabirii', 'footballoo', 'criktoo', 'tvoo', 'familytimeoo', 'fishingoo', 'cpmuteroo', 'huntingoo', 'trevlingoo', 'shoppingoo', 'sleepingoo', 'cheessoo', 'musicoo'),
(3, '', 'Football', '', 'Watching TV', '', '', 'Computer', '', '', '', '', '', ''),
(4, 'Imran Kabir', 'Football', '', 'Watching TV', 'Family Time', '', 'Computer', '', '', 'Shopping', '', 'Chees', ''),
(5, 'Nafee', '', '', '', '', '', 'Computer', '', 'Traveling', '', '', '', ''),
(6, 'PHP7', '', '', '', '', '', 'Computer', '', '', 'Shopping', '', '', ''),
(7, 'Rajib', 'Football', 'Cricket', 'Watching TV', 'Family Time', 'Fishing', 'Computer', 'Hunting', 'Traveling', 'Shopping', 'Sleeping', 'Chees', 'Listening to Music'),
(8, 'Rajib', 'Football', 'Cricket', 'Watching TV', 'Family Time', 'Fishing', 'Computer', 'Hunting', 'Traveling', 'Shopping', 'Sleeping', 'Chees', 'Listening to Music'),
(10, 'Shumee', '', '', '', 'Family Time', '', '', '', 'Traveling', 'Shopping', 'Sleeping', 'Chees', ''),
(12, 'Shumee', '', '', '', '', '', 'Computer', '', '', 'Shopping', '', '', ''),
(14, 'Manik', 'Football', 'Cricket', '', '', 'Fishing', '', 'Hunting', 'Traveling', '', 'Sleeping', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `it_books`
--

CREATE TABLE IF NOT EXISTS `it_books` (
  `id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `it_books`
--

INSERT INTO `it_books` (`id`, `text`, `name`) VALUES
(35, 'Rood to Democracy.888', 'Jhon Smith77999'),
(36, 'Learning Computer', 'Sadman Kabir'),
(37, 'Advance Learning English', 'Amy Blackwood'),
(38, 'Basic Cricket Rules', 'Adnan kabir'),
(39, 'How to Be a Good Police Officer.', 'Raihan Kabir'),
(40, 'Ghost Rider. ', 'Nafee'),
(41, 'Hajar Bochor Dhore7777', 'Jasmin Akther Chowdhury888'),
(42, 'Object oriented Programming in PHP', 'Imran Kabir');

-- --------------------------------------------------------

--
-- Table structure for table `list_of_genders`
--

CREATE TABLE IF NOT EXISTS `list_of_genders` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `list_of_genders`
--

INSERT INTO `list_of_genders` (`id`, `title`, `name`) VALUES
(11, 'Male', 'Mahmudur Rahman chy'),
(12, 'Female', 'Amy Blackwood'),
(17, 'Female', 'Rehnuma Chy Rubin'),
(18, 'Female', 'Korimunnesa'),
(19, 'Female', 'Jasmin Akther Chowdhury');

-- --------------------------------------------------------

--
-- Table structure for table `profile_pic`
--

CREATE TABLE IF NOT EXISTS `profile_pic` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_pic`
--

INSERT INTO `profile_pic` (`id`, `title`, `image`) VALUES
(28, 'Imran', 'Brown_and_Blue_Transparent_Butterfly_Clipart.png');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `name`, `title`) VALUES
(2, 'Imran Kabir66', '11ekrajib@hotmail.com'),
(3, 'Imran Kabir', 'ekrajib@hotmail.com'),
(5, 'Rajib', 'kjhkf@kljslf.com'),
(6, 'Imran Kabir', 'asdasddd@jlda.com'),
(9, 'PHP7', 'ikr22015@gmail.com'),
(10, 'PHP7', 'ikr_2013@yahoo.com'),
(12, 'Imran Kabir', 'ekrajib@hotmail.com'),
(13, 'Imran Kabir', 'ekrajib@hotmail.com'),
(14, 'Imran Kabir', 'asdasd@jlda.com'),
(15, 'Imran Kabir66', 'asdasd@jlda.com');

-- --------------------------------------------------------

--
-- Table structure for table `summaries`
--

CREATE TABLE IF NOT EXISTS `summaries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summaries`
--

INSERT INTO `summaries` (`id`, `name`, `title`) VALUES
(1, 'Agencyweaver Associates999', 'Web Development Company999'),
(2, 'Agencyweaver Associates', 'Web Development Company'),
(3, 'Agencyweaver Associates', 'Web Development Company'),
(4, 'Agencyweaver Associates', 'Web Development Company'),
(5, 'Imran Kabir', 'asdasdqqqqqqqqqqqqqqq'),
(6, 'Imran Kabir', 'asdasdqqqqqqqqqqqqqqq'),
(7, 'Imran Kabir', 'asdasdqqqqqqqqqqqqqqq'),
(8, 'Rajib', 'gggggggggggggggggggggggggggg'),
(9, 'PHP7', 'pppppppppppppppppppppp'),
(13, 'OOOOOOOOrg', 'qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE IF NOT EXISTS `terms` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `title`) VALUES
(1, 'agree'),
(2, 'Agree'),
(3, 'Agree'),
(4, 'Agree'),
(5, 'Agree'),
(6, 'Agree'),
(7, 'Agree');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bday`
--
ALTER TABLE `bday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `birthdays`
--
ALTER TABLE `birthdays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookmarks`
--
ALTER TABLE `bookmarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `it_books`
--
ALTER TABLE `it_books`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `list_of_genders`
--
ALTER TABLE `list_of_genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_pic`
--
ALTER TABLE `profile_pic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summaries`
--
ALTER TABLE `summaries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bday`
--
ALTER TABLE `bday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=121;
--
-- AUTO_INCREMENT for table `birthdays`
--
ALTER TABLE `birthdays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `bookmarks`
--
ALTER TABLE `bookmarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `it_books`
--
ALTER TABLE `it_books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `list_of_genders`
--
ALTER TABLE `list_of_genders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `profile_pic`
--
ALTER TABLE `profile_pic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `summaries`
--
ALTER TABLE `summaries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
