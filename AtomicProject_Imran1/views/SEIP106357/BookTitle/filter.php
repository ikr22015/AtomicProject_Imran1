<?php
	include_once("../../../vendor/autoload.php");

	use Imran\BITM\SEIP106357\booktitle\Text;
	use Imran\BITM\SEIP106357\Utility\Utility;
	
	$book = new Text();
	$books = $book->filter($_REQUEST['filter']);
	//Utility::dd($books);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>
	<link rel="stylesheet" href="../../../resource/css/style.css" media="screen" title="no title" charset="utf-8">
    <!-- Bootstrap -->
	<link href="../../../resource/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="wrapper">
		<div id="header" class="page-header">
			<h1><a href="index.php">Book Title</a></h1>
			<nav class="navbar navbar-inverse">
				<ul>
					<li><a class="active" href="index.php">Home</a></li>
					<li><a href="../../../index.php">Go to Ptoject Navigation Page</a></li>
				</ul>
			</nav>
		</div><!-- #header -->
		<div id="jumbotron" class="jumbotron">
		  <div class="container">
			<div class="booklist-sub-container">
				<div class="list-nav">
					<ul>
						<li>
							<form action="filter.php" method="get">
								<input type="number" name="filter" placeholder="Search by ID...">
								<button type="submit" name="" value="">Search</button>
							</form>
						</li>
						<li><a href="">Download</a></li>
						<li><a href="create.php">Add New Book</a></li>
					</ul>
				</div>
				<div id="profile-sum-org-panel-success" class="panel panel-primary">
					<div id="profile-sum-org-panel-heading" class="panel-heading"><p style="margin:0">Book List</p>
					</div>
					<div>
						<?php echo Utility::message(); ?>
					</div>
					<form action="" method="">
						<table class="table table-bordered" border="1">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Book Title</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><?php echo $books->id;?></td>
									<td style="text-align:left"><?php echo $books->name ?></td>
									<td style="text-align:left"><?php echo $books->text ?></td>
									<td>
										<button type="button" name="button" value=""><a href="show.php?id=<?php echo $books->id;?>">View</a></button>
										<input type="hidden" name="id" value="<?php echo $book->id; ?>">
										<button type="button" name="button" value=""><a href="edit.php?id=<?php echo $books->id;?>">Edit</a></button>
									
										<button type="button" name="button" value=""><a href="delete.php?id=<?php echo $books->id;?>">Delete</a></button>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
					<div class="paginating">
						<nav>
						  <ul class="pagination">
							<li>
							  <a href="#" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							  </a>
							</li>
							<li><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li>
							  <a href="#" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							  </a>
							</li>
						  </ul>
						</nav>
					</div>
				</div>
			</div>
		  </div>
		</div>
		<div id="footer" class="page-header">
			<p>
				&copy; Mohammad Emran Kabir. SEID-106357. PHP Batch-11
			</p>
		</div><!-- #footer -->
	</div><!-- #wrapper -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script>

	</script>
  </body>
</html>
