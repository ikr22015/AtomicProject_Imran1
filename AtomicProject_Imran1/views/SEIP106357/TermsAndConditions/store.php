<?php
	include_once("../../../vendor/autoload.php");
	
	use Imran\BITM\SEIP106357\TermsAndConditions\CheckboxSingle;
	use Imran\BITM\SEIP106357\Utility\Utility;
	
	$terms = new CheckboxSingle($_POST);
	$terms->store();
?>